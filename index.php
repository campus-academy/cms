<?php
include_once("assets/php/db.php");
session_start();
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8"/>
    <title>Campus-Academy Blog</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css"
          integrity="sha256-46r060N2LrChLLb5zowXQ72/iKKNiw/lAmygmHExk/o=" crossorigin="anonymous"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/js/all.min.js"
            integrity="sha256-+Q/z/qVOexByW1Wpv81lTLvntnZQVYppIL1lBdhtIq0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="shortcut icon" type="image/png" href="assets/img/fav-icon.png">
</head>
<body>

<div id="wrap">
    <header class="header">
        <nav class="nav">
            <a href="#wrap" id="open">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                     y="0px" width="34px" height="27px" viewBox="0 0 34 27" enable-background="new 0 0 34 27"
                     xml:space="preserve">
              <rect fill="#4e57aa" width="34" height="4"/>
                    <rect y="11" fill="#4e57aa" width="34" height="4"/>
                    <rect y="23" fill="#4e57aa" width="34" height="4"/>
            </svg>
            </a>
            <a href="#" id="close">×</a>
            <h1>Campus Academy Blog</h1>
            <a href="blog">Blog</a>
            <a href="createpost">Nouveau post</a>
            <?php
            if (isset($_SESSION['userRank'])) {
                if ($_SESSION['sessionState'] && $_SESSION['userRank'] == 1) {
                    echo "<a href='param'>Paramètre d'affichage</a>";
                }
            }
            ?>
            <a href="presentation">A propos</a>
            <?php
            if (isset($_SESSION['sessionState'])) {
                if ($_SESSION['sessionState'] !== true) {
                    echo "<a href='auth'>Connexion</a>";
                } else {
                    echo "<a href='auth' onclick='logout()'>Déconnexion</a>";
                }
            } else {
                echo "<a href='auth'>Connexion</a>";
            }
            ?>

        </nav>
    </header>

  <main id="main-block" class="main">
    <?php
      if (isset($_REQUEST["page"])) {
        switch ($_REQUEST["page"]) {
          case 'blog':
            include_once("assets/php/blog.php");
            break;
          case 'create-post':
            include_once("assets/php/create-post.php");
            break;
          case 'param':
            include_once("assets/php/param.php");
            break;
          case 'presentation':
            include_once("assets/php/presentation.php");
            break;
          case 'auth':
            break;
          default :
            include_once("assets/php/presentation.php");
        }
      }

        ?>
    </main>
</div>
<?php
if (isset($_REQUEST["page"])) {
switch ($_REQUEST["page"]) {
    case 'auth':
        include_once("assets/php/auth.php");
        break;
    }
  }
?>

<script src="assets/js/jquery-3.5.1.min.js"></script>
<script src="assets/js/script.js" type="text/javascript"></script>
</body>
</html>

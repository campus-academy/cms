<?php
  if (!isset($_SESSION['userRank']) || !isset($_SESSION['sessionState'])) {

    header('Location: presentation');
    exit;
  } elseif ($_SESSION['userRank'] != 1) {

    header('Location: presentation');
    exit;
  }

?>

<h1>Modification des paramètres d'affichage</h1>

<form class="form-param" action="#" method="post">

  <fieldset>
    <legend>Titre de catégorie</legend>

    <input type="color" name="titleCat-color" value="#ffffff">
    <label for="titleCat-color">Couleur</label><br/>

    <input type="color" name="titleCat-back_color" value="#cccccc">
    <label for="titleCat-back_color">Couleur de fond</label><br/>

    <label for="titleCat-size">Taille</label>
    <input type="number" name="titleCat-size" value="24"><br/>

    <fieldset>
      <legend>Decoration</legend>
      <input type="checkbox" name="titleCat-italic" value="">
      <label for="titleCat-italic">Italique</label><br/>
      <input type="checkbox" name="titleCat-bold" value="">
      <label for="titleCat-bold">Gras</label></br>
      <input type="checkbox" name="titleCat-underline" value="">
      <label for="titleCat-underline">Soulignement</label><br/>
    </fieldset>

    </select>
  </fieldset><br/>

  <fieldset>
    <legend>Titre de post</legend>

    <input type="color" name="titlePost-color">
    <label for="titlePost-color">Couleur</label><br/>

    <input type="color" name="titlePost-back_color" value="#ffffff">
    <label for="titlePost-back_color">Couleur de fond</label><br/>


    <label for="titlePost-size">Taille</label>
    <input type="number" name="titlePost-size" value="16"><br/>

    <fieldset>
      <legend>Decoration</legend>
      <input type="checkbox" name="titlePost-italic" value="">
      <label for="titlePost-italic">Italique</label><br/>
      <input type="checkbox" name="titlePost-bold" value="">
      <label for="titlePost-bold">Gras</label></br>
      <input type="checkbox" name="titlePost-underline" value="">
      <label for="titlePost-underline">Soulignement</label><br/>
    </fieldset>

    </select>
  </fieldset><br/>

  <fieldset>
    <legend>Texte des post</legend>

    <input type="color" name="textPost-color">
    <label for="textPost-color">Couleur</label><br/>

    <input type="color" name="textPost-back_color" value="#ffffff">
    <label for="textPost-back_color">Couleur de fond</label><br/>

    <label for="textPost-size">Taille</label>
    <input type="number" name="textPost-size" value="12"><br/>

    <fieldset>
      <legend>Decoration</legend>
      <input type="checkbox" name="textPost-italic" value="">
      <label for="textPost-italic">Italique</label><br/>
      <input type="checkbox" name="textPost-bold" value="">
      <label for="textPost-bold">Gras</label></br>
      <input type="checkbox" name="textPost-underline" value="">
      <label for="textPost-underline">Soulignement</label><br/>
    </fieldset>

    </select>
  </fieldset>

  <button type="button" name="button" onclick="changeParam()">Effectuer les changements</button>
</form

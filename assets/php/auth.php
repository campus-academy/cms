
<div class="modal auth">
    <a href="blog" class="auth-exit">x</a>
    <div id="form-response"></div>
    <div class="left">
        <div class="logo"></div>
        <span class="auth-text">La page de connexion n'a pas pu être finalisé, car les images ne voulaient pas apparaître à cause du htaccess !</span>
        <div class="auth-img"></div>
    </div>
    <div class="right">
        <div id="form-response"></div>
        <div id="login-form" class="form">
            <p class="auth-title">Connexion</p>
            <input id="loginInput" class="login" name="login" type="text" placeholder="Votre login"/>
            <input id="passwordInput" class="password" name="password" placeholder="Mot de passe" type="password"/>
            <button name="submit" id="login-form-button" class="gradient-button" onclick="login()">
                <span class="text">CONNEXION</span>
            </button>
            <span class="toggleAuthForm">Je n'ai pas de compte</span>
        </div>
        <div id="register-form" class="form">
            <p class="auth-title">Inscription</p>
            <input id="loginInput" class="login" name="login" type="text" placeholder="Votre login"/>
            <input id="passwordInput" class="password" name="password" placeholder="Mot de passe" type="password"/>
            <button name="submit" id="login-form-button" class="gradient-button" onclick="register()">
                <span class="text">Inscription</span>
            </button>
            <span class="toggleAuthForm">Je me connecte</span>
        </div>
    </div>
</div>
/*******************************************************************************
 AJAX
 *******************************************************************************/

var xmlTimeOut = 10000;

/////// Chargement des données d'activité
function ajaxFunction(myScript, myParam) {

    var xmlhttpPrincipalList;
    if (window.XMLHttpRequest) xmlhttpPrincipalList = new XMLHttpRequest();
    else xmlhttpPrincipalList = new ActiveXObject("Microsoft.XMLHTTP");
    xmlhttpPrincipalList.onreadystatechange = function () {

        if (xmlhttpPrincipalList.readyState == 4 && xmlhttpPrincipalList.status == 200) {

            $('#main-block').html(xmlhttpPrincipalList.responseText);

        // $('#main-block').send();
        /*console.log(xmlhttpPrincipalList.responseText);
        console.log(xmlhttpPrincipalList);
        console.log(JSON.stringify(xmlhttpPrincipalList.responseText));*/
        return xmlhttpPrincipalList.responseText;

        }
    }
    xmlhttpPrincipalList.open("POST", myScript, true);
    xmlhttpPrincipalList.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=utf8");
    xmlhttpPrincipalList.timeout = xmlTimeOut;
    xmlhttpPrincipalList.ontimeout = function () {
        xmlhttpPrincipalList.abort();
        xmlhttpPrincipalList = null;
    }
    xmlhttpPrincipalList.send(myParam);
}

/*******************************************************************************
AJAX UTILISATION
*******************************************************************************/

function checkUser() {
    let listUser = ajaxFunction('./assets/php/ajax.php', "Fajax=get-user");
}

function login() {
    var login = $("#login-form #loginInput").val();
    var password = $("#login-form #passwordInput").val();
    var response = ajaxFunction('assets/php/ajax.php', "Fajax=login&login=" + login + "&password=" + password);
    $('#form-response').append(response);
}

function register() {
    var login = $("#register-form #loginInput").val();
    var password = $("#register-form #passwordInput").val();
    var response = ajaxFunction('assets/php/ajax.php', "Fajax=register&login=" + login + "&password=" + password);
    $('#form-response').html(response);
}

function logout() {
    const response = ajaxFunction('assets/php/ajax.php', "Fajax=logout");
}

$('#register-form .toggleAuthForm').on('click', function () {
    $('#login-form').show();
    $('#register-form').hide();
})

$('#login-form .toggleAuthForm').on('click', function () {
    $('#login-form').hide();
    $('#register-form').css('display', 'flex');
})

function changeParam(){
  var param = "";
  param += "&titleCat-color="+$("[name=titleCat-color]").val();
  param += "&titleCat-back_color="+$("[name=titleCat-back_color]").val();
  param += "&titleCat-size="+$("[name=titleCat-size]").val();
  param += "&titleCat-italic="+$("[name=titleCat-italic]").is(":checked");
  param += "&titleCat-bold="+$("[name=titleCat-bold]").is(":checked");
  param += "&titleCat-underline="+$("[name=titleCat-underline]").is(":checked");

  param += "&titlePost-color="+$("[name=titlePost-color]").val();
  param += "&titlePost-back_color="+$("[name=titlePost-back_color]").val();
  param += "&titlePost-size="+$("[name=titlePost-size]").val();
  param += "&titlePost-italic="+$("[name=titlePost-italic]").is(":checked");
  param += "&titlePost-bold="+$("[name=titlePost-bold]").is(":checked");
  param += "&titlePost-underline="+$("[name=titlePost-underline]").is(":checked");

  param += "&textPost-color="+$("[name=textPost-color]").val();
  param += "&textPost-back_color="+$("[name=textPost-back_color]").val();
  param += "&textPost-size="+$("[name=textPost-size]").val();
  param += "&textPost-italic="+$("[name=textPost-italic]").is(":checked");
  param += "&textPost-bold="+$("[name=textPost-bold]").is(":checked");
  param += "&textPost-underline="+$("[name=textPost-underline]").is(":checked");

  console.log(param);
  ajaxFunction('assets/php/ajax.php', "Fajax=changeParam"+param);
}

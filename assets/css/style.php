<?php

header('content-type: text/css');
header('Cache-Control: max-age=31536000, must-revalidate')

//impossible de faire fonctionner ce bout de code car dès que nous utilisons autre choses
//qu'un echo, le fichier meurt
//include_once ('assets/php/db.php');
//// It's not working because php
//
//try {
//    $sql = "SELECT * FROM styles";
//    $sql = $bdd->prepare($sql);
//    $sql->execute();
//
//    var_dump($sql->fetchAll(PDO::FETCH_ASSOC));
//    while ($row = $sql->fetchAll(PDO::FETCH_ASSOC)) //outputs the records
//    {
//        var_dump($row);
//        echo "
//      ." . $row['name'] . "{
//        color: " . $row['color'] . ";
//        background-color: " . $row['back_color'] . ";
//        font-size: " . $row['size'] . "px;";
//        if ($row['italic']) {
//            echo "font-style: italic;";
//        }
//        if ($row['bold']) {
//            echo "font-weight: bold;";
//        }
//        if ($row['underline']) {
//            echo "text-decoration: underline;";
//        }
//        echo "}";
//    };
//} catch (PDOException $e) {
//    echo die(json_encode($e->getMessage()));
//}
?>

body {
  margin: 0;
  background-color: #eff0f2;
}

.title-cat{
  background-color: #4e57aa;
  color: white;
  text-align: center;
  font-size: <?php echo $bibi; ?>;
}

#wrap {
  font-family: sans-serif;
  font-size: 21px;
  line-height: 1.6;
  margin: 0;
  display: flex;
  justify-content: flex-end;
  color: #3e3d3f;
  transition: transform .4s cubic-bezier(.25, .1, .25, 1);
  height: 100vh;
}

#wrap:not(:target) {
transform: translate3d(-335px, 0, 0);
}

#wrap:target {
transform: translate3d(0, 0, 0);
}

#open,
#close {
height: 44px;
text-align: right;
display: block;
font-size: 50px;
margin-top: -30px;
margin-right: -30px;
}

#wrap:target #open,
#wrap:not(:target) #close {
display: none;
}

#wrap:target #open {
display: none;
}

.nav a {
color: #4e57aa;
text-decoration: none;
display: block;
}

.header {
background: white;
width: 360px;
padding: 0 20px;
display:inline-block;
vertical-align:top;
height: 100%;
}

.nav {
padding: 25px;
}

.main {
padding: 10px 50px;
margin: 15px 400px;
flex: 1;
display:inline-block;
max-height: 100vh;
max-width: 100vw;
overflow-y: scroll;
background: white;
}

.main::-webkit-scrollbar {
display: none;
}

p {
max-width: 1200px;
color: #3e3d3f;
text-align: justify;
}

/* Login/Register */

.modal.auth {
position: absolute;
display: flex;
z-index: 9999;
left: 0;
top: 0;
right: 0;
bottom: 0;
background-color: #ffffff;
}

.modal.auth .left,
.modal.auth .right {
flex: 1;
}

.modal.auth .left {
background-image: url("../img/auth-back.svg");
background-size: cover;
background-color: purple;
}

.modal.auth .right {
display: flex;
align-items: center;
justify-content: center;
}

.logo {
background-image: url("../img/logo-ca.svg");
height: 150px;
width: 150px;
}

.auth-title {
font-family: Avenir, sans-serif;
font-size: 40px;
}

.auth-img {
background-image: url("../img/auth-img.png");
background-size: contain;
}
.form {
display: flex;
flex-direction: column;
}

#register-form {
display: none;
}

.toggleAuthForm{
margin-top: 0.5em;
text-decoration: underline;
text-align: center;
cursor: pointer;
transition: 0.5s all;
}
.toggleAuthForm:hover{
color: #c3547e;
}

#login-form input,
#register-form input {
border: none;
border: 1px solid #c3547e;
opacity: 1;
font-size: 26px;
border-radius: 25px;
padding: 15px 25px;
margin: 1em;
}

input::placeholder {
color: black;
opacity: 0.5;
}



input:hover {
transition: all 0.3s linear;
}

input:focus {
transition: all 0.3s linear;
outline: none;
}

.gradient-button {
margin: 0 auto;
border: none;
font-size: 20px;
padding: 15px;
text-transform: uppercase;
transition: 0.5s;
background-size: 200% auto;
color: #FFF;
width: 200px;
transition: all 0.3s cubic-bezier(.25,.8,.25,1);
cursor: pointer;
border-radius: 25px;
background-image: linear-gradient(to right, #c3547e 0%, #513bac 51%)
}

.auth-exit {
position: absolute;
font-size: 30px;
color: black;
font-family: Arial, sans-serif;
text-decoration: none;
right: 1em;
top: 1em;
}

.auth-exit:hover {
    color: #513bac;
transform: rotate(360deg);
transition: 0.5s;
color: red;
}
